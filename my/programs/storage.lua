require("/my/libs/fileLib")
require("/my/libs/output")
require("/my/libs/storageLib")


local output = Output.new();
local log = function (text) 
    output:log(text)
end
if(#arg < 1) then
    output:error("Usage count, get")
end



local function get()
    local matInfo = readObjFromFile("/storageInfo")
    local toGet = {}
    local search = nil;

    local getMatching = function()
        local res = {}
        for k,v in pairs(matInfo) do
            if(search == nil or string.find(k,search)) then
                res[k] = v
            end
        end
        return res
    end
    local reprint = function()
        term.clear()

        print "******* Available: *******"
        local m = getMatching()
        for k,v in pairs(m) do
            print (k.." - "..v)
        end

        print "******* List: *******"
        for k,v in pairs(toGet) do
            print (k.." - "..v)
        end
        print("*** Search: "..(search or ""))
    end

    while (true) do
        reprint()

        local event, key, isHeld = os.pullEvent("key")
        if(isHeld ==false)then
            if(key == 28) then
                local m = getMatching()
                local getting = nil
                for k,v in pairs(m) do
                    getting = k
                    break
                end

                if(getting ~= nil) then
                    print(getting)
                    write("How many: ")
                    local num = read()
                    toGet[getting] = (toGet[getting] or 0) + num
                    search = nil
                end
            elseif(key == 57) then
                return toGet
            elseif(key == 14) then
                search = string.sub(search,1, -2)
            elseif(key == 211) then
                search = nil
            elseif(key == 1) then
                break                
            else
                search = (search or "")..(keys.getName(key))
            end
        end
    end 
end





mode=arg[1]
if(mode == "count") then
    local writeFunc = function ( text, type)
        print(textutils.serialize(text))
        if(text.storageInfo) then
            writeObjToFile(text.storageInfo,"/storageInfo")
        end
    end
    getMatterials(nil,writeFunc)

elseif(mode == "get") then
    local toGet = get()
    local writeFunc = function ( text, type)
        print(textutils.serialize(text))
    end
    getMatterials(toGet,writeFunc)

else 
    output:error("Usage count, get")
end










