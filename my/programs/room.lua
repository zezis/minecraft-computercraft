require("/my/libs/moveLib")
w=arg[1]
l=arg[2]

local dumpL = {x= 363, y = 1850, z=0, o =0}
local notDumpObjects = {"minecraft:torch", "minecraft:coal", 'c', 'd'}
local origLoc = readLocation()
local tochSteps = 20
local torchCounter = 0


if(math.mod(w,2)==1) then
	error("Width has to be even!")
end 


function dumpAndReturn()
	local l = readLocation()
	
	first = "y"
	second = "x"
	if(math.mod(origLoc.o+4,2) == 0) then
		first = "x"
		second = "y"
	end

	goToLocation(dumpL,false,first,second,"z")
	for i=1,16,1 do
		turtle.select(i)
		local data = turtle.getItemDetail()
		if (data and arrayHasValue(notDumpObjects,data.name) == false) then
			turtle.dropDown()
		end
	end	
	goToLocation(l,false,second,first,"z")
end

function placeTorch()
	for i=1,16,1 do
		turtle.select(i)
		local data = turtle.getItemDetail()
		if data and data.name=="minecraft:torch" then
			turtle.placeDown()
			return
		end
	end
end

function refuel()
	for i=1,16,1 do
		turtle.select(i)
		local data = turtle.getItemDetail()
		if data then 
			print(data.name)
		end
		if data and data.name=="minecraft:coal" then
			turtle.refuel(10)
			return
		end
	end
end

function saveDigDownUp()
	local success,data = turtle.inspectDown()
	if (success == false or arrayHasValue(notDumpObjects,data.name) == false) then
		turtle.digUp()
		turtle.digDown()
	end
	if (success and data.name == "minecraft:torch") then
		torchCounter = 0
	end
end

function step()
	torchCounter = torchCounter+1	
	if(torchCounter >= tochSteps) then
		torchCounter = 0
		placeTorch()
	end
	
	if(turtle.getFuelLevel() < 1000) then
		refuel()
	end

    while (forward() ==false) do
        turtle.dig()
    end
    saveDigDownUp()
    return    
end


for curW=1,w,1
do
    for curL=1,l-1,1    
    do
        step()
    end

    if(math.mod(curW,2)==1) then
        turnLeft()
        step()
        turnLeft()
    else 
		dumpAndReturn()
        turnRight()
        step()
        turnRight()
    end
end
