require("/my/libs/location")
local locationFilePath = "/locations"

function readLocation()
	local loc = {}
	file = fs.open (locationFilePath,"r")
	loc.x=tonumber(file.readLine())
	loc.y=tonumber(file.readLine())
	loc.z=tonumber(file.readLine())
	loc.o=tonumber(file.readLine())
	file.close()
	
	return Location.new(loc)
end

function writeLocation(loc)
	file = fs.open (locationFilePath,"w")
	file.writeLine(loc.x)
	file.writeLine(loc.y)
	file.writeLine(loc.z)
	file.writeLine(loc.o)
	file.close()
end


function up()
	local loc = readLocation()
	loc.z = loc.z+1
	result = turtle.up()
	if(result) then
		writeLocation(loc)
	end
	return result
end

function down()
	local loc = readLocation()
	loc.z = loc.z-1
	result = turtle.down()
	if(result) then
		writeLocation(loc)
	end
	return result
end

function forward() 
	local loc = readLocation()
	
	if(loc.o == 0) then --north 
		loc.y=loc.y-1
	end
	if(loc.o == 2) then --south
		loc.y=loc.y+1 
	end
	if(loc.o == 1) then --east
		loc.x=loc.x+1
	end
	if(loc.o == 3) then --west
		loc.x=loc.x-1 
	end
	
	result = turtle.forward()

	if(result) then
		writeLocation(loc)
	end
	return result
end


function back() 
	local loc = readLocation()
	
	if(loc.o == 0) then --north 
		loc.y=loc.y+1
	end
	if(loc.o == 2) then --south
		loc.y=loc.y-1 
	end
	if(loc.o == 1) then --east
		loc.x=loc.x-1
	end
	if(loc.o == 3) then --west
		loc.x=loc.x+1 
	end
	
	result = turtle.back()

	if(result) then
		writeLocation(loc)
	end
	return result
end

function turnLeft() 
	local loc = readLocation()	

	loc.o = math.mod(loc.o-1+4,4)
	result = turtle.turnLeft()
	
	if(result) then
		writeLocation(loc)
	end
	return result
end

function turnRight() 
	local loc = readLocation()	

	loc.o = math.mod(loc.o+1+4,4)
	result = turtle.turnRight()
	
	if(result) then
		writeLocation(loc)
	end
	return result
end

function turnTo(orientation)
	orientation = math.mod(orientation+4,4)
	
	local curr = readLocation().o

	if(math.mod(curr + 2,4) == orientation) then
		turnLeft()
		turnLeft()
	elseif(math.mod(curr+1+4,4) == orientation) then
		turnRight()
	elseif(math.mod(curr-1+4,4) == orientation) then
		turnLeft()
	end 
end

function goToLocation(loc, force, a1, a2, a3)
	
	local curr = readLocation()

	if(a1 == nil or a1 == "x") then 
		goToLocationX(curr,loc,force)
	elseif a1 == "y" then
		goToLocationY(curr,loc,force)
	elseif a1 == "z" then
		goToLocationZ(curr,loc,force)
	else 
		error("Wrong direction")
	end
	
	
	if(a2 == nil or a2 == "y") then 
		goToLocationY(curr,loc,force)
	elseif a2 == "x" then
		goToLocationX(curr,loc,force)
	elseif a2 == "z" then
		goToLocationZ(curr,loc,force)
	else  
		error("Wrong direction")
	end
	
	if(a3 == nil or a3 == "z") then 
		goToLocationZ(curr,loc,force)
	elseif a3 == "x" then
		goToLocationX(curr,loc,force)
	elseif a3 == "y" then
		goToLocationY(curr,loc,force)
	else  
		error("Wrong direction")
	end
		
	turnTo(loc.o)
end


function goToLocationX(curr,loc,force)
	if(curr.x> loc.x) then 
		turnTo(3)
	else
		turnTo(1)
	end	
		
	for x = 1,math.abs(curr.x-loc.x),1 do
		while(forward()==false) do
			if(force == true) then
				turtle.Dig()
			end
		end
	end
end

function goToLocationY(curr,loc,force)
	if(curr.y> loc.y) then 
		turnTo(0)
	else
		turnTo(2)
	end	
	
	for y = 1,math.abs(curr.y-loc.y),1 do
		while(forward()==false) do
			if(force == true) then 
				turtle.Dig() 
			end
		end
	end
end

function goToLocationZ(curr,loc,force)
	for z = 1,math.abs(curr.z-loc.z),1 do
		if(curr.z > loc.z) then
			while(down()==false) do
				if(force == true) then 
					turtle.DigDown() 
				end
			end
		else
			while(up()==false) do
				if(force == true) then 
					turtle.DigUp() 
				end
			end
		end
	end
end



function arrayHasValue (arr, val)
    for index, value in ipairs(arr) do
        if value == val then
            return true
        end
    end

    return false
end



