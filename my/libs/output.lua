
Output = {
    __private= {
        write = function(text,type)
            print(os.time().."|"..type.. "|"..text)
        end
    },
    LogType = "Log",
    WarningType = "Warnign",
    ErrorType = "Error"
}

function Output.new(writeFunc)
    local d = {}
    setmetatable(d,{__index = Output})
    if(writeFunc ~= nil) then
        d.__private.write = writeFunc
    end
    return d
end

function Output:log(text)
    local p = self.__private
    p.write(text, Output.LogType)
end

function Output:warnign(text)
    local p = self.__private
    p.write(text, Output.WarningType)
end

function Output:error(text)
    local p = self.__private
    p.write(text, Output.ErrorType)
    error(text)
end
