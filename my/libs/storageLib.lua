require("/my/libs/moveLib")
require("/my/libs/fileLib")
require("/my/libs/output")

local settings = readObjFromFile("/settings").storage

-- local objToGet = {}
-- objToGet["minecraft:dirt"] = 150
-- objToGet["minecraft:cobblestone"] = 65


function getInventoryInfo()
    local info = {}
    for i=1,16,1 do
        local data = turtle.getItemDetail(i)
        info[i] = {}
        info[i].i = i
        if (data) then
            info[i].data = data
        end
    end
    return info
end

function getInventoryInfoByMat()
    local info = getInventoryInfo()
    local res = {}
    for k,v in pairs(info) do
        if(v.data ~= nil) then
            plus = 0
            old = res[v.data.name]
            if(old ~=nil) then
                plus = old
            end
            res[v.data.name] = v.data.count + plus
        end
    end
    return res
end

function getFreeSlot()
    local info = getInventoryInfo()
    for k,v in pairs(info) do
        if(v.data == nil) then
           return k
        end
    end
    return nil
end


function dumpCollected(oAlreadyDropped, report)
    report("Running to dump...")

    local currLoc = readLocation()
    local outputLoc = settings.outputLoc;

    down()

    local first =  "y"
    local second = "x"
    if(math.mod(settings.storageO+4,2) == 0) then
        first = "x"
        second = "y"
    end
     
    goToLocation(outputLoc,false, first,second,"z")
    
    local info = getInventoryInfo()
    for k,v in pairs(info) do
        if(v.data and v.data.name ~= "ironchest:iron_chest") then
            oAlreadyDropped[v.data.name] = (oAlreadyDropped[v.data.name] or 0) +v.data.count
            turtle.select(k)
            turtle.dropUp()
        end
    end
    down()
    goToLocation(currLoc,false,second,first,"z")
end

function isDone(oDropped,oToGet)
    local inv =  getInventoryInfoByMat()
    for k,need in pairs(oToGet) do
        local have = (oDropped[k] or 0) + (inv[k] or 0)
        if(have < need) then
            return false
        end 
    end
    
    return true
end








function getMatterials(oToGet, writeFunc)
    local storageInfo = {}
    local oAlreadyDropped = {}
    local getMaterials = true;
    if(oToGet == nil) then
        getMaterials = false
    end

    local output = Output.new(writeFunc);
    local log = function (text, storageInfo) 
        local o = {message: text}
        if(storageInfo) then
            o.storageInfo = storageInfo
        end
        output:log(o)
    end


    --go to start (end on left side)
    log("Going to start...")
    turnTo(settings.storageO-1)
    while(turtle.detect()==false) do
        forward()
    end

    --start
    log("Starting...")
    turnTo(settings.storageO+1)

    local done = false
    local chestNum = 1
    while(true) do
        local chest = peripheral.wrap("top")
        log("Searching "..chestNum..". chest")
        chestNum = chestNum +1

        for i=1, chest.size(),1 do
            data = chest.getItemMeta(i)

            if(data) then 
                if(getMaterials and oToGet[data.name]) then
                    local have = (getInventoryInfoByMat()[data.name] or 0) + (oAlreadyDropped[data.name] or 0)
                    local needStill = oToGet[data.name] - have
                    local toGet = math.min(needStill,data.count)
                    if(toGet > 0)then
                        freeSlot = getFreeSlot()
                        if(freeSlot == nil) then
                            dumpCollected(oAlreadyDropped)
                        end
                        local got = chest.pushItems("down",i,toGet,freeSlot)
                        if(got ~= toGet)then
                            log("I wanted "..toGet..", but got only "..got.." from slot "..i)
                        else
                            log("I already got "..toGet.." of "..data.name)                
                        end

                        if(needStill == got and isDone(oAlreadyDropped,oToGet)) then
                            done = true
                            log("Done earlier...")
                            break
                        end
                    end
                end
                storageInfo[data.name] = (storageInfo[data.name] or 0) + data.count
            end
        end


        if(turtle.detect() or (done and getMaterials)) then
            break
        else 
            forward()
        end
    end


    if(getMaterials) then 
        dumpCollected(oAlreadyDropped,log)
    end

    log("Done", storageInfo)
end


--countObjects(objToGet)