function readObjFromFile(path)
    local file = fs.open (path,"r")
    local text = file.readAll()
    file.close()

    return textutils.unserialize(text)
end

function writeObjToFile(obj, path)
    local text = textutils.serialize(obj)
    local file = fs.open (path,"w")
    file.write(text)
    file.close()
end
