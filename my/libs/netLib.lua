
function send(data, channel, returnChannel)
    local modem = peripheral.find("modem")
    if(modem == nil) then
        error("No modem connected")
    end
    modem.transmit(channel, returnChannel, data)
end



function listen(channel)
    local modem = peripheral.find("modem")
    if(modem == nil) then
        error("No modem connected")
    end


    modem.open(channel)  -- Open channel 3 so that we can listen on it
    local event, modemSide, senderChannel, 
    replyChannel, message, senderDistance = os.pullEvent("modem_message")

    -- print("I just received a message on channel: "..senderChannel)
    -- print("I should apparently reply on channel: "..replyChannel)
    -- print("The modem receiving this is located on my "..modemSide.." side")
    -- print("The message was: "..textutils.serialize(message))
    -- print("The sender is: "..(senderDistance or "an unknown number of").." blocks away from me.")

    return  message, event, modemSide, senderChannel, replyChannel, senderDistance
end