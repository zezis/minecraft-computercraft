baseURL = "https://bitbucket.org/zezis/minecraft-computercraft/raw/HEAD"

local p = http.get(baseURL.."/paths/init_paths") --Get contents of page

local paths = {}
local i = 1

while(true) do
    local text = p.readLine()
    if(text == nil) then
        break
    else
        paths[i] = text
        i = i+1
    end
end
p.close() --Just in case

for k,v in pairs(paths) do
    if(v and v~= "") then
        print("About to write: "..v)
        local netFile = http.get((baseURL..v))
        
        local file = fs.open (v,"w")
        file.write(netFile.readAll())
        file.close()

        print("Wrote: "..v)
        netFile.close()
    end
end
