require("my/libs/output")
require("my/libs/fileLib")
require("my/libs/netLib")


local sett = readObjFromFile("settings")

local output = Output.new()
local log = function (text) 
    output:log(text)
end

--Disc runner
local currPath = shell.getRunningProgram()
if(currPath == "disk/startup") then
    log("Starting as disc init script")
    fs.copy(currPath, "startup")
    log("Copy done")
    shell.run("startup")
    log("Program started")
    return
end




log("Starting as "..sett.type)



if(sett.type == "listener") then
    local message = listen(3)
    if(message) then
        if(message.type=="get from storage") then
            countObjects(message.objects)
        end
    end

end
 
 
 if(sett.type == "commander") then
    send(sett)
    
    while(true) do
        local msg = listen(3)
        log(msg)
    end 
        
end
