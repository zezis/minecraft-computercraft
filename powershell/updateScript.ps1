$initItems = Get-ChildItem ../ -Exclude "update_paths","init_paths","netInstall.lua", "*.ps1" -Recurse -File | Resolve-Path -Relative

$result = ""
foreach ($i in $initItems){
    $pom = ($i).Remove(0,2).Replace("\", "/")
    $result += "$pom`r`n"
}
$result | Out-File ..\paths\init_paths -Encoding ascii




$updateItems = Get-ChildItem ../ -Exclude "update_paths","init_paths","netInstall.lua", "*.ps1","settings","locations", "storageInfo" -Recurse -File | Resolve-Path -Relative

$result = ""
foreach ($i in $updateItems){
    $pom = ($i).Remove(0,2).Replace("\", "/")
    $result += "$pom`r`n"
}
$result | Out-File ..\paths\update_paths -Encoding ascii

