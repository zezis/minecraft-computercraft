
$path = "C:\Users\zezul\Twitch\Minecraft\Instances\FTB Ultimate Reloaded\saves\Creative\computer"
$folders = Get-ChildItem $path -Directory
$toCopy = Get-ChildItem ../ -Exclude "settings"

foreach ($folder in $folders.name){
  #  Write-Host $folder
    #For each folder in C:\banana, copy folder c:\copyme and it's content to c:\banana\$folder

    $toDelete = Get-ChildItem "$path\$folder" -Exclude "settings","locations", "storageInfo"
    foreach ($td in $toDelete.name){
        Write-Host "Deleting $path\$folder\$td"
        Remove-Item "$path\$folder\$td" -Recurse
    }


    $toCopy = Get-ChildItem ../ -Exclude  "settings","locations","paths", "powershell"
    foreach ($tc in $toCopy.name){
        Write-Host "Copying $tc"
        Copy-Item -Path "../$tc" -Destination "$path\$folder" -Recurse
    }
}

# $toCopy = Get-ChildItem -Exclude "settings" 
# foreach ($tc in $toCopy.name){
#     Write-Host $tc
# }
